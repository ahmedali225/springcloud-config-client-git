package com.spring.springcloud.configclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcloudConfigClientGitApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudConfigClientGitApplication.class, args);
    }

}

